//
//  VolumeView.swift
//  Conversion
//
//  Created by Hariharan S on 01/05/24.
//

import SwiftUI

struct VolumeView: View {
    @State private var inputValue: Double = 0
    @State private var selectedInputUnit: VolumeUnitType = .none
    @State private var selectedOutputUnit: VolumeUnitType = .none
    @FocusState private var isInputTextFieldFocused: Bool
    
    var outputValue: String {
        guard let inputUnit = self.selectedInputUnit.unit,
              let outputUnit = self.selectedOutputUnit.unit
        else {
            return "0"
        }
        let inputValue = Measurement(value: self.inputValue, unit: inputUnit)
        let outputValue = inputValue.converted(to: outputUnit)
        return "\(outputValue)"
    }
    
    var body: some View {
        NavigationStack {
            Form {
                Section("Enter the input value") {
                    TextField("0", value: self.$inputValue, format: .number)
                        .keyboardType(.decimalPad)
                        .focused(self.$isInputTextFieldFocused)
                }
                
                Section {
                    Picker("Select the Input Unit", selection: self.$selectedInputUnit) {
                        ForEach(VolumeUnitType.allCases, id: \.self) {
                            Text($0.rawValue.capitalized)
                        }
                    }
                }
                
                Section {
                    Picker("Select the Output Unit", selection: self.$selectedOutputUnit) {
                        ForEach(VolumeUnitType.allCases, id: \.self) {
                            Text($0.rawValue.capitalized)
                        }
                    }
                }
                
                Section("Output Value") {
                    Text(self.outputValue)
                }
            }
            .navigationTitle("Volume Conversion")
            .toolbar {
                if self.isInputTextFieldFocused {
                    Button("Done") {
                        self.isInputTextFieldFocused = false
                    }
                }
            }
        }
    }
}

#Preview {
    VolumeView()
}
