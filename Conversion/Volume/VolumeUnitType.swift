//
//  VolumeUnitType.swift
//  Conversion
//
//  Created by Hariharan S on 01/05/24.
//

import Foundation

enum VolumeUnitType: String, CaseIterable {
    case none
    case milliliter
    case centiliter
    case liter
    case kiloliter
    case teaSpoon
    case tableSpoon
    case cup
    case gallon
    
    var unit: UnitVolume? {
        switch self {
        case .milliliter:
           return .milliliters
        case .liter:
            return .liters
        case .cup:
            return .cups
        case .teaSpoon:
            return .teaspoons
        case .tableSpoon:
            return .tablespoons
        case .gallon:
            return .gallons
        case .kiloliter:
            return .kiloliters
        case .centiliter:
            return .centiliters
        default:
            return nil
        }
    }
}
