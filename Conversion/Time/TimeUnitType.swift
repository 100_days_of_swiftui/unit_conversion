//
//  TimeUnitType.swift
//  Conversion
//
//  Created by Hariharan S on 01/05/24.
//

import Foundation

enum TimeUnitType: String, CaseIterable {
    case none
    case microsecond
    case milisecond
    case second
    case minutes
    case hour
    
    var unit: UnitDuration? {
        switch self {
        case .microsecond:
            return .microseconds
        case .milisecond:
            return .milliseconds
        case .second:
           return .seconds
        case .minutes:
            return .minutes
        case .hour:
            return .hours
        default:
            return nil
        }
    }
}
