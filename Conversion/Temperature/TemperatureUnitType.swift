//
//  TemperatureUnitType.swift
//  Conversion
//
//  Created by Hariharan S on 01/05/24.
//

import Foundation

enum TemperatureUnitType: String, CaseIterable {
    case none
    case celsius
    case fahrenheit
    case kelvin
    
    var unit: UnitTemperature? {
        switch self {
        case .celsius:
                .celsius
        case .fahrenheit:
                .fahrenheit
        case .kelvin:
                .kelvin
        default:
            nil
        }
    }
}
