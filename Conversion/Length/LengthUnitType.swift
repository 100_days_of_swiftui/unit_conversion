//
//  LengthUnitType.swift
//  Conversion
//
//  Created by Hariharan S on 01/05/24.
//

import Foundation

enum LengthUnitType: String, CaseIterable {
    case none
    case milimeter
    case centimeter
    case meter
    case kilometer
    case inch
    case feet
    case yard
    case mile
    
    var unit: UnitLength? {
        switch self {
        case .milimeter:
           return .millimeters
        case .centimeter:
            return .centimeters
        case .meter:
            return .meters
        case .kilometer:
            return .kilometers
        case .inch:
            return .inches
        case .feet:
            return .feet
        case .yard:
            return .yards
        case .mile:
            return .miles
        default:
            return nil
        }
    }
}
