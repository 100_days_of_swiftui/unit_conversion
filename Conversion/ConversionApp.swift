//
//  ConversionApp.swift
//  Conversion
//
//  Created by Hariharan S on 01/05/24.
//

import SwiftUI

@main
struct ConversionApp: App {
    var body: some Scene {
        WindowGroup {
//            TimeView()
            TemperatureView()
//            LengthView()
//            VolumeView()
        }
    }
}
